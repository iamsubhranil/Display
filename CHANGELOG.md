Changelog
=========

1.0 (2018-06-11)
----------------

#### New
- Initial implementation. [Subhranil Mukherjee]
