# Display
#### A library in C for displaying colored characters and messages to the terminal

This is a no-dep, minimal, efficient library for printing colored texts in C.

### Files

1. `display.h` : Library interface.

2. `display.c` : Implementation of the library.

3. `main.c` : An example usage of the library by a driver.

### API

All methods support usual `printf` style variadic arguments.

1. `pdbg`, `pwarn`, `perr`, `pinfo` : Show various levels of status messages to the user.

3. `pred`, `pblue`, `pgrn`, `pylw`, `pcyn`, `pmgn` : Print colored text.

2. `ph*` : Print colored header only, followed by uncolored text.

4. `ANSI_COLOR_*` macros : Using any of them in a string or `printf` will result rendering of all following strings in that specific color, 
until a `ANSI_COLOR_RESET` is found.

5. `ANSI_FONT_BOLD` macro : Using it will result rendering of all following strings in *bold*, until a `ANSI_COLOR_RESET` is found.
