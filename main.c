#include "display.h"

typedef void(*colfunc)(const char*, ...);

int main(){
    pdbg("This is a debug message!");
    pwarn("This is a warning!");
    perr("This is an error!");
    pinfo("This is an info!");

    phred("\n\n[Red] ", "Don't you think red headers are cool?");
    phblue("\n[Blue] ", "How about this blue then?");
    phgrn("\n[Green] ", "Alright you can't go wrong with this one :>");
    phylw("\n[Yellow] ", "You don't think so?");
    phmgn("\n[Magenta] ", "Fine, here's another one!");
    phcyn("\n[Cyan] ", "Here's the blast that was saved for the last!");

    pblue("\n\nYou");
    pred(" can");
    pgrn(" print");
    pylw(" colored");
    pcyn(" texts");
    pmgn(" too!");

    pblue(ANSI_FONT_BOLD "\n\nYou");
    pred(ANSI_FONT_BOLD " can");
    pgrn(ANSI_FONT_BOLD " also");
    pylw(ANSI_FONT_BOLD " bold");
    pcyn(ANSI_FONT_BOLD " them");
    pmgn(ANSI_FONT_BOLD " happily!\n\n");
    
    colfunc cols[] = {&pblue, &pred, &pgrn, &pylw, &pcyn, &pmgn};
    
    const char *str = "You can do anything you want!\n"
                        "Your imagination is the only limit!";

    int count = sizeof(cols)/sizeof(colfunc);
    for(int i = 0;str[i] != '\0';i++){
        cols[i % count]("%c", str[i]);
    }
    
    pblue("\n");
    return 0;
}
